import * as Realm from 'realm-web'

export default {
    login: async () => {
        const REALM_KEY = process.env.NEXT_PUBLIC_REALM_KEY;
        const app = new Realm.App({id: REALM_KEY});
        
        const credential = await Realm.Credentials.anonymous();
        try {
            if(app.currentUser) {
                console.log(app.currentUser.id);
                return app.currentUser; 
            } else {
                const credential = await Realm.Credentials.anonymous();
                const user = await app.logIn(credential);
                console.log(user.id)
                return user;
            }
        } catch (error) {
            console.log(error);
            return null;
        }
    }
}